﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// This class represents a receipt of a withdrawl that contains
    /// the status of the withdrawl and the bills withdrawn.
    /// </summary>
    public class WithdrawlReceipt
    {
        public enum WithdrawlStatus
        {
            OK,
            InsufficientFunds,
            Error
        }

        /// <summary>
        /// Status of the withdrawl
        /// </summary>
        public WithdrawlStatus Status { get; set; }
        
        /// <summary>
        /// The bills that were withdrawn, if any
        /// </summary>
        public IList<Money> Bills { get; set; }

        /// <summary>
        /// The amount withdrawn
        /// </summary>
        public int Amount { get; set; }

        public WithdrawlReceipt()
        {
            Bills = new List<Money>();
            Status = WithdrawlStatus.Error;  // Be pessimistic here, let instantiater set correct value
        }
    }
}
