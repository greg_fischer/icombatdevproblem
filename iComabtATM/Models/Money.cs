﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// This class represents a bill of money
    /// </summary>
    public abstract class Money
    {

        public Guid SerialNumber { get; set; }
        public abstract int Amount { get; }

        public Money()
        {
            this.SerialNumber = Guid.NewGuid();
        }

    }
}
