﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Bills
{
    public class HundredMoney : Money
    {
        public override int Amount
        {
            get
            {
                return 100;
            }
        }

        public HundredMoney() : base()
        {

        }
    }
}
