﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Bills
{
    public class FiftyMoney : Money
    {
        public override int Amount
        {
            get
            {
                return 50;
            }
        }

        public FiftyMoney() : base()
        {

        }
    }
}
