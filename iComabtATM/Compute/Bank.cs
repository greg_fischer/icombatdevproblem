﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Common.Exceptions;

namespace Compute
{
    public class Bank : IBank
    {
        public List<Money> Ones { get; set; }
        public List<Money> Fives { get; set; }
        public List<Money> Tens { get; set; }
        public List<Money> Twenties { get; set; }
        public List<Money> Fifties { get; set; }
        public List<Money> Hundreds { get; set; }

        public Bank()
        {
            Ones = new List<Money>(10);
            Fives = new List<Money>(10);
            Tens = new List<Money>(10);
            Twenties = new List<Money>(10);
            Fifties = new List<Money>(10);
            Hundreds = new List<Money>(10);
        }

        public IList<Money> GetStock()
        {
            List<Money> all = new List<Money>();
            all.AddRange(Ones);
            all.AddRange(Fives);
            all.AddRange(Tens);
            all.AddRange(Twenties);
            all.AddRange(Fifties);
            all.AddRange(Hundreds);
            return all;
        }

        public void Reset()
        {
            // bye bye whatever is in there!
            Ones.Clear();
            Fives.Clear();
            Tens.Clear();
            Twenties.Clear();
            Fifties.Clear();
            Hundreds.Clear();

            // put em all back
            int max = 10;
            for(int i=0; i<max; i++)
            {
                Ones.Add(Factory.CreateOne());
                Fives.Add(Factory.CreateFive());
                Tens.Add(Factory.CreateTen());
                Twenties.Add(Factory.CreateTwenty());
                Fifties.Add(Factory.CreateFifty());
                Hundreds.Add(Factory.CreateHundred());
            }
        }

        public WithdrawlReceipt WithdrawMoney(int amount)
        {
            // set up our vars!
            int curAmount = amount;
            WithdrawlReceipt receipt = new WithdrawlReceipt();
            List<Money> toDispense = null;

            try
            {
                while (curAmount > 0)
                {
                    // start at the top.  if the bill isn't too big, and
                    // we have some, use one from that pile.  if not,
                    // then on to the next one
                    if (curAmount >= 100 && Hundreds.Count > 0)
                    {
                        toDispense = Hundreds;
                    }
                    else if (curAmount >= 50 && Fifties.Count > 0)
                    {
                        toDispense = Fifties;
                    }
                    else if (curAmount >= 20 && Twenties.Count > 0)
                    {
                        toDispense = Twenties;
                    }
                    else if (curAmount >= 10 && Tens.Count > 0)
                    {
                        toDispense = Tens;
                    }
                    else if (curAmount >= 5 && Fives.Count > 0)
                    {
                        toDispense = Fives;
                    }
                    else if (curAmount >= 1 && Ones.Count > 0)
                    {
                        toDispense = Ones;
                    }

                    if (toDispense == null)
                    {
                        // wooooah, we gots a problem!  No stack of bills to pull from!
                        receipt.Status = WithdrawlReceipt.WithdrawlStatus.InsufficientFunds;
                        // you get nothing!  put those bills back
                        RefundReceipt(receipt);
                        receipt.Amount = 0;
                        // we're done here
                        break;
                    }
                    else
                    {
                        // get our bill, add to receipt, remove from pile
                        Money ourGuy = toDispense[0];
                        receipt.Bills.Add(ourGuy);
                        receipt.Status = WithdrawlReceipt.WithdrawlStatus.OK;
                        receipt.Amount = receipt.Amount + ourGuy.Amount;
                        curAmount = curAmount - ourGuy.Amount;
                        toDispense.RemoveAt(0);
                    }

                    toDispense = null;  // reset
                } // while
            }
            catch (UnexpectedDenominationException)
            {
                receipt.Status = WithdrawlReceipt.WithdrawlStatus.Error;  //TODO:  set a receipt error message?
            }
            catch(NullReferenceException)
            {
                receipt.Status = WithdrawlReceipt.WithdrawlStatus.Error;
            }
            catch (IndexOutOfRangeException)
            {
                receipt.Status = WithdrawlReceipt.WithdrawlStatus.Error;
            }

            return receipt;
        }

        /// <summary>
        /// puts the money in the passed receipt back into the
        /// bank.  this is usually because we were in the middle
        /// of a withdrawl but had to abort
        /// </summary>
        /// <param name="receipt">The receipt whose bills
        /// will be put back into stock</param>
        private void RefundReceipt(WithdrawlReceipt receipt)
        {
            foreach (var curMoney in receipt.Bills)
            {
                switch(curMoney.Amount)
                {
                    case 1:
                        Ones.Add(curMoney);
                        break;
                    case 5:
                        Fives.Add(curMoney);
                        break;
                    case 10:
                        Tens.Add(curMoney);
                        break;
                    case 20:
                        Twenties.Add(curMoney);
                        break;
                    case 50:
                        Fifties.Add(curMoney);
                        break;
                    case 100:
                        Hundreds.Add(curMoney);
                        break;
                    default:
                        // this should never happen
                        throw new UnexpectedDenominationException(String.Format("Unknown denomination {0} while refunding receipt.", curMoney.Amount));
                }
            }
        }
    }
}
