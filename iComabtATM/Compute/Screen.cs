﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Compute
{
    public class Screen : IScreen
    {
        public void printAllStock(IList<Money> bills)
        {
            Console.Out.WriteLine("Machine Balance:");
            printStock(bills);
        }

        public void printReceipt(WithdrawlReceipt receipt)
        {
            // compose message based on status
            string toDisplay = String.Empty;
            if(receipt.Status == WithdrawlReceipt.WithdrawlStatus.OK)
            {
                toDisplay = String.Format("Success: Dispensed ${0}", receipt.Amount);
            }
            else if(receipt.Status == WithdrawlReceipt.WithdrawlStatus.InsufficientFunds)
            {
                toDisplay = "Withdrawl Failure:  Insufficient funds";
            }
            else
            {
                toDisplay = "Unable to process your request.  Please try again.";
            }
            Console.WriteLine(toDisplay);
        }

        public void printStock(IList<Money> bills)
        {
            // group all these bills by denomination so we can get a count of each.  
            var groups = bills.GroupBy(c => c.Amount).OrderBy(g => g.Key);
            foreach(var curGroup in groups)
            {
                Console.WriteLine(String.Format("${0} - {1}", curGroup.Key, curGroup.Count()));
            }
        }
    }
}
