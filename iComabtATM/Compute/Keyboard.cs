﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compute
{
    public class Keyboard : IKeyboard
    {
        public IAtm Atm { get; set; }

        public Keyboard()
        {
            Atm = ComputeFactory.CreateAtm();
        }

        public void run()
        {
            // don't stop till they tell us
            while(true)
            {
                string input = Console.ReadLine();
                string command = String.Empty;
                string args = String.Empty;

                // lets get the command our lovely user gave us
                if(String.IsNullOrWhiteSpace(input) == false)
                {
                    // parse out the command from its args
                    command = input.Substring(0, 1);
                    if(input.Length > 1)
                    {
                        args = input.Substring(1).Trim();
                    }

                    // if they didn't quit, process command
                    if (String.Compare(command, "Q", true) == 0)
                    {
                        break;
                    }
                    else
                    {
                        processCommand(command, args);
                    }
                }
                else
                {
                    printInvalidInput();
                }
            }
        }

        /// <summary>
        /// Prints in invalid command message
        /// </summary>
        private void printInvalidInput()
        {
            Console.WriteLine("Invalid Command.");
        }

        /// <summary>
        /// Processes the passed input command using the passed arguments.
        /// if the command is not recognizd (R, W, I) then an error
        /// message is displayed.
        /// </summary>
        /// <param name="cmd">Command entered by user</param>
        /// <param name="args">Parameters for command entered by user</param>
        private void processCommand(string cmd, string args)
        {
            if(String.Compare(cmd, "R", true) == 0)
            {
                processRestock(args);
            }
            else if (String.Compare(cmd, "W", true) == 0)
            {
                processWithdrawl(args);
            }
            else if (String.Compare(cmd, "I", true) == 0)
            {
                processInquiry(args);
            }
            else
            {
                printInvalidInput();
            }
        }

        /// <summary>
        /// processes the R restock command to restock the ATM to default levels
        /// </summary>
        /// <param name="args">Arguments entered by user, should be empty</param>
        private void processRestock(string args)
        {
            if(String.IsNullOrWhiteSpace(args))
            {
                Atm.Restock();
            }
            else
            {
                printInvalidInput();
            }
        }

        /// <summary>
        /// Processes the W withdrawl command that will take money
        /// out of ATM.
        /// </summary>
        /// <param name="args">The args entered by user, should be amount</param>
        private void processWithdrawl(string args)
        {
            // have the method do all the hard work
            int cashAmt = parseCashInput(args);

            // if they entered good value, go to work.  otherwise tell them
            // of their blunder
            if (cashAmt > 0)
            {
                Atm.Withdraw(cashAmt);
            }
            else
            {
                printInvalidInput();
            }
        }

        /// <summary>
        /// Processes the I print stock command, which will print what
        /// is in the ATM for the entered denominations
        /// </summary>
        /// <param name="args">Args entered by user, should be denominations of bills</param>
        private void processInquiry(string args)
        {
            List<int> denominations = new List<int>();
            if(String.IsNullOrWhiteSpace(args) == false)
            {
                // parse each entered denomination.  should be separated by space
                string[] rawDenoms = args.Split(' ');
                // if user only entered one value, the array will be empty.  we already checked they entered
                // something, so if array empty, add what we have, which SHOULD be a single entry, but below
                // code will handle that
                if(rawDenoms.Length < 1)
                {
                    rawDenoms = new string[] { args };
                }

                foreach(var curDenom in rawDenoms)
                {
                    int converted = parseCashInput(curDenom.Trim());
                    if(converted > 0)
                    {
                        // woooooo, user ain't stupid!
                        denominations.Add(converted);
                    }
                    else
                    {
                        // ugh, read the freaking manual.  stop parsing, clear out any entered values thus far
                        denominations.Clear();
                        break;
                    }
                }
            }

            if(denominations.Count > 0)
            {
                // we got something.  go to work
                Atm.PrintStock(denominations);
            }
            else
            {
                printInvalidInput();
            }
        }

        /// <summary>
        /// Will parse the inputted cash amount into an integer.
        /// <para>
        /// The input must be in the format $XXX, where XXX is an integer
        /// </para>
        /// </summary>
        /// <param name="input">inputted parameter</param>
        /// <returns>parsed cash value, or -1 if unable</returns>
        private int parseCashInput(string input)
        {
            int cashAmt = -1;
            // check for crazy input
            if (String.IsNullOrWhiteSpace(input) == false && input.IndexOf("$") == 0)
            {
                // parse their input to number                
                Int32.TryParse(input.Substring(1), out cashAmt);
            }
            return cashAmt;
        }

    }
}
