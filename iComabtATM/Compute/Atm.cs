﻿using Common;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compute
{
    public class Atm : IAtm
    {
        public IBank MoneyBank { get; set; }
        public IScreen UI { get; set; }

        public Atm()
        {
            MoneyBank = ComputeFactory.CreateBank();
            MoneyBank.Reset();
            UI = ComputeFactory.CreateScreen();
        }

        public void PrintStock(IList<int> denominations)
        {
            // first, get all inventory
            IList<Money> cash = MoneyBank.GetStock();

            // now, snatch the ones we're looking for
            IList<Money> targets = cash.Where(c => denominations.Contains(c.Amount)).ToList();

            // print bills
            UI.printStock(targets);
        }

        public void Restock()
        {
            MoneyBank.Reset();
            PrintAllBills();
        }

        public void Withdraw(int amount)
        {
            // tell the bank gimmee the caaaaaaasssshhhh
            WithdrawlReceipt receipt = MoneyBank.WithdrawMoney(amount);
            // display result
            UI.printReceipt(receipt);

            // now, if we had a good withdraw, update our fine user on what's
            // still in the ATM
            if(receipt.Status == WithdrawlReceipt.WithdrawlStatus.OK)
            {
                // get what we got, print it
                PrintAllBills();
            }
        }

        /// <summary>
        /// Will print all bills in ATM
        /// </summary>
        private void PrintAllBills()
        {
            UI.printAllStock(MoneyBank.GetStock());
        }
    }
}
