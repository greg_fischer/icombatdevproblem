﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compute
{
    public static class ComputeFactory
    {
        /// <summary>
        /// Creates a new keyboard object
        /// </summary>
        /// <returns>new keyboard</returns>
        public static IKeyboard CreateKeyboard()
        {
            return new Keyboard();
        }

        /// <summary>
        /// Creates a new ATM object
        /// </summary>
        /// <returns>new ATM</returns>
        public static IAtm CreateAtm()
        {
            return new Atm();
        }

        /// <summary>
        /// Creates a new Bank object
        /// </summary>
        /// <returns>new Bank</returns>
        public static IBank CreateBank()
        {
            return new Bank();  // would love some DI here
        }

        /// <summary>
        /// Creates a new Screen object
        /// </summary>
        /// <returns>new Screen</returns>
        public static IScreen CreateScreen()
        {
            return new Screen();
        }
    }
}
