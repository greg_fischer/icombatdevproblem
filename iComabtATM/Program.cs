﻿using Common;
using Compute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iComabtATM
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("**********************************************************");
            Console.WriteLine("   Welcome to the ICombat ATM.  Enter your input below.   ");
            Console.WriteLine("**********************************************************");
            Console.WriteLine();
            IKeyboard keyboard = ComputeFactory.CreateKeyboard();
            keyboard.run();
        }
    }
}
