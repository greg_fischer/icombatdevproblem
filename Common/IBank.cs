﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public interface IBank
    {
        /// <summary>
        /// Will withdraw the passed amount from the bank,
        /// returning a receipt with status and result.
        /// </summary>
        /// <param name="amount">The amount to withdraw</param>
        /// <returns>Object containing status/result</returns>
        WithdrawlReceipt WithdrawMoney(int amount);

        /// <summary>
        /// Resets the banks available funds to the default
        /// amounts.
        /// </summary>
        void Reset();

        /// <summary>
        /// Returns a list of all bills currently stocked in 
        /// the machine.
        /// </summary>
        /// <returns>List of bills present in machine</returns>
        IList<Money> GetStock();
    }
}
