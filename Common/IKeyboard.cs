﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    /// <summary>
    /// Classes that implement this interface listen for keyboard
    /// commands from the user, then responds accordingly.
    /// </summary>
    public interface IKeyboard
    {
        /// <summary>
        /// Listens for, and respond to, inputs from user, until
        /// they quit.
        /// </summary>
        void run();
    }
}
