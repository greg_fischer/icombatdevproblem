﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    /// <summary>
    /// Classes that implement this interface can print statuses,
    /// results, and other important items to the UI
    /// </summary>
    public interface IScreen
    {
        /// <summary>
        /// Prints the receipt to the screen, which will display
        /// the result of a withdraw
        /// </summary>
        /// <param name="receipt">The receipt for the withdrawl</param>
        void printReceipt(WithdrawlReceipt receipt);

        /// <summary>
        /// Prints the amount of each bill passed
        /// </summary>
        /// <param name="bills">The lis of bills</param>
        void printStock(IList<Money> bills);

        /// <summary>
        /// Prints all the bills in the machine
        /// </summary>
        /// <param name="bills">All bills in machine</param>
        void printAllStock(IList<Money> bills);
    }
}
