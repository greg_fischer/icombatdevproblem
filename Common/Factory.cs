﻿using Models;
using Models.Bills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    /// <summary>
    /// This factory instantiates classes
    /// </summary>
    public static class Factory
    {

        /// <summary>
        /// Creates a One dollar bill
        /// </summary>
        /// <returns>One dollar money</returns>
        public static Money CreateOne()
        {
            return new OneMoney();
        }

        /// <summary>
        /// Creates a Five dollar bill
        /// </summary>
        /// <returns>Five dollar money</returns>
        public static Money CreateFive()
        {
            return new FiveMoney();
        }

        /// <summary>
        /// Creates a Ten dollar bill
        /// </summary>
        /// <returns>Ten dollar money</returns>
        public static Money CreateTen()
        {
            return new TenMoney();
        }

        /// <summary>
        /// Creates a Twenty dollar bill
        /// </summary>
        /// <returns>Twenty dollar money</returns>
        public static Money CreateTwenty()
        {
            return new TwentyMoney();
        }

        /// <summary>
        /// Creates a Fifty dollar bill
        /// </summary>
        /// <returns>Fifty dollar money</returns>
        public static Money CreateFifty()
        {
            return new FiftyMoney();
        }

        /// <summary>
        /// Creates a Hundred dollar bill
        /// </summary>
        /// <returns>Hundred dollar money</returns>
        public static Money CreateHundred()
        {
            return new HundredMoney();
        }
    }
}
