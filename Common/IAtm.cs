﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public interface IAtm
    {
        /// <summary>
        /// Will withdraw the passed amount and display the
        /// status of the withdrawl, and the remaining funds.
        /// </summary>
        /// <param name="amount">Amount to withdraw</param>
        void Withdraw(int amount);

        /// <summary>
        /// Restocks the ATM with the default amounts
        /// </summary>
        void Restock();

        /// <summary>
        /// Will print the amount of stocked bills with
        /// the passed denominations
        /// </summary>
        /// <param name="denominations">List of bill denominations</param>
        void PrintStock(IList<int> denominations);
    }
}
